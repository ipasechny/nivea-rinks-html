// const path = require('path');
const sortCSSmq = require('sort-css-media-queries');

function em(px, base) {
  base = base ? base : 10;
  return round((px / base) + 0.0001, 4);
}

function round(value, decimals) {
  return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

module.exports = {
  plugins: [
    require('postcss-import'),
    require('postcss-mixins'),

    require('postcss-simple-vars'),
    require('postcss-nested-ancestors'),
    require('postcss-nested'),

    require('postcss-preset-env')({
      stage: 0,
      preserve: false
    }),

    require('postcss-calc')({
      precision: 5,
      mediaQueries: true
    }),

    //require('postcss-url')([
    //	{
    //		url: 'inline',
    //		filter: /\.woff2$/,
    //		basePath: path.resolve(__dirname, 'source/fonts')
    //	}
    //]),

    require('postcss-functions')({
      functions: {
        px: function (px) {
          return px + 'px';
        },
        em: function (px, base) {
          return em(px, base) + 'em';
        },
        rem: function (px, base) {
          return em(px, base) + 'rem';
        },
        lh: function (px, base) {
          return em(px, base);
        },
      },
    }),

    require('postcss-conditionals'),

    require('rucksack-css')({
      alias: false,
      clearFix: false,
      responsiveType: false,
      shorthandPosition: false
    }),

    require('cssnano')({
      preset: [
        'default', {
          autoprefixer: true,
          discardComments: {
            removeAll: true,
          },
        }
      ]
    }),

    require('postcss-functions')({
      functions: {
        percent: function (value) {
          return value + '%';
        },
      }
    }),

    require('css-mqpacker')({
      sort: sortCSSmq.desktopFirst,
    }),

    require('autoprefixer')({
      grid: 'autoplace',
    }),

  ]
}