var VK_APP_ID = '6410422';
var FB_APP_ID = '2034480560127675';

var MAIN_PATH = $('.lMain').data('pf-proxy-url');

var TEST_HOST = 'https://azext-eu.nivea.ru/';

var TEST_PATH = location.hostname !== 'localhost' ? '/nivea_katki/' : '';
var TEST_URI = TEST_HOST + TEST_PATH;

var VOTE_URI = 'https://www.nivea.ru' + MAIN_PATH + 'social/social_redirect.php';
var TEST_VOTE_URI = 'https://azext-eu.nivea.ru/nivea_katki/social/social_redirect.php';

var IS_BASE = MAIN_PATH !== '/';

var BASE_PATH = IS_BASE ? MAIN_PATH : TEST_PATH;
var BASE_HOST = IS_BASE ? 'https://www.nivea.ru' : TEST_HOST;
var BASE_AJAX = BASE_HOST + BASE_PATH + 'include/ajax.php';

var PAGE_HASH = location.hash;
var PAGE_PATH = location.pathname;

var RU_CENTER = [61.698653, 99.505405];
var RU_BOUNDS = [
  [41.18599, 19.484764],
  [81.886117, 191.128003]
];

window.dataLayer = window.dataLayer || [];

function wordnum(num, words) {
  var cases = [2, 0, 1, 1, 1, 2];

  return words[
    (num % 100 > 4 && num % 100 < 20)
      ? 2 : cases[(num % 10 < 5) ? num % 10 : 5]
  ];
}

function firePixels(name) {
  if (window.fbq) {
    fbq('track', name);
  }
  if (window.VK) {
    VK.Retargeting.Event(name);
  }
}

function fireAnalytics(action, category) {
  dataLayer.push({
    event: 'ga_event',
    eventAction: action,
    eventCategory: category,
  });

  	ym(66990547, 'reachGoal', action, {category: category});
}

// #region [maps]
if (window.ymaps) {
  var map;
  var mapOM;
  var mapPin;
  var mapForm = document.getElementById('map_form');
  var mapRink = document.getElementById('map_rink');

  ymaps.ready(function () {

    // #region [map_form]
    if (mapForm) {
      (function () {
        var $input = $('#coords');
        var pinHandler = function () {
          map.events.add('click', function (e) {
            var coords = e.get('coords');

            $input.val(coords).valid();

            $('.pMap_close').addClass('_done');

            if (mapPin) {
              mapPin.geometry.setCoordinates(coords);
            } else {
              mapPin = createPin(coords, true);

              map.geoObjects.add(mapPin);

              mapPin.events.add('dragend', function () {
                var coords = mapPin.geometry.getCoordinates();

                $input.val(coords).valid();
              });
            }
          });
        };

        ymaps.geolocation.get({
          provider: 'yandex',
          mapStateAutoApply: true,
        })
          .then(
            function (res) {
              var itm = res.geoObjects.get(0);
              var center = itm.geometry.getCoordinates();
              var bounds = itm.properties.get('boundedBy');

              if (itm.getCountry() === 'Россия') {
                createMap('map_form', center, bounds, pinHandler);
              } else {
                createMap('map_form', RU_CENTER, RU_BOUNDS, pinHandler);
              }
            },
            function (e) {
              createMap('map_form', RU_CENTER, RU_BOUNDS, pinHandler);
            }
          );
      }());
    }
    // #endregion [map_form]

    // #region [map_rink]
    if (mapRink) {
      (function () {
        var center = [
          parseFloat(mapRink.getAttribute('data-x')),
          parseFloat(mapRink.getAttribute('data-y'))
        ];

        createMap('map_rink', center, false, function () {
          mapPin = createPin(center, false);

          map.geoObjects.add(mapPin);
        });
      }());
    }
    // #endregion [map_rink]

  });

  var fillMap = function () {
    mapOM = new ymaps.ObjectManager({
      gridSize: 80,
      clusterize: true,
    });

    mapOM.objects.options.set({
      iconImageSize: [35, 46],
      iconImageOffset: [-18, -46],
      iconLayout: 'default#image',
      iconImageHref: BASE_PATH + 'assets/img/design/ico/pin-1.svg',
    });

    mapOM.clusters.options.set({
      openBalloonOnClick: false,
      clusterIconColor: '#00136f',
      preset: 'islands#darkBlueClusterIcons',
    });

    map.geoObjects.add(mapOM);

    $.ajax({
      type: 'post',
      url: BASE_AJAX,
      dataType: 'json',
      data: {
        mode: 'get-markers',
      },
      success: function (data) {
        mapOM.add(data);
      },
    });
  };
  var createPin = function (coords, isDrag) {
    return (
      new ymaps.Placemark(coords, {}, {
        zIndex: 1000,
        draggable: isDrag,
        iconImageSize: [45, 52],
        iconImageOffset: [-23, -52],
        iconLayout: 'default#image',
        iconImageHref: BASE_PATH + 'assets/img/design/ico/pin-2.svg',
      })
    );
  };
  var createMap = function (id, center, bounds, callback) {
    map = new ymaps.Map(id, {
      zoom: 15,
      center: center,
      bounds: bounds,
      controls: [
        'zoomControl',
        'typeSelector',
        'searchControl',
        'geolocationControl'
      ],
    }, {
      checkZoomRange: true,
      typeSelectorFloat: 'left',
      searchControlSize: 'small',
      restrictMapArea: RU_BOUNDS,
      searchControlProvider: 'yandex#search',
    });

    map.setCenter(center);

    /*
    ymaps.geocode('Россия').then(function(res) {
      var itm = res.geoObjects.get(0),
        itmCenter = itm.geometry.getCoordinates(),
        itmBounds = itm.properties.get('boundedBy');

      map.setBounds(RU_BOUNDS, {
        checkZoomRange: true
      });
    });
    */

    map.events.add('click', function () {
      map.balloon.close();
    });

    fillMap();

    if (typeof callback === 'function') {
      callback();
    }
  };
}

function mapScrollZoomOn() {
  if (window.map) {
    window.map.behaviors.enable('scrollZoom');
  }
}

function mapScrollZoomOff() {
  if (window.map) {
    window.map.behaviors.disable('scrollZoom');
  }
}
// #endregion [maps]

(function ($) {
  var isMobile;
  var isDesktop;

  var fbToken = !1;

  var userIP = window.userip || null;

  var thisDate = new Date();
  var szMobile = 'screen and (max-width: 1280px)';

  var $pMap = $('#pMap');
  var $mMain = $('.mMain');
  var $mPages = $('#mPages');
  var $lLoader = $('#lLoader');
  var $lOverlay = $('#lOverlay');
  var $ajaxOutput = $('#ajax_output');

  var $pThx = $('#pThx').remodal();
  var $pNot = $('#pNot').remodal();
  var $pCreate = $('#pCreate').remodal();
  var $pFeedback = $('#pFeedback').remodal();
  var $pCheckAdded = $('#pCheckAdded').remodal();

  var $formNotify = $('.js_form_error');

  var $bday = $('#bday');

  var $fRinks = $('#fRinks');
  var $fRinksType = $('#fRinks_type');
  var $fRinksCity = $('#fRinks_city');
  var $fRinksCityID = $('#fRinks_city_id');
  var $fRinksSortType = $('#fRinks_sort_type');
  var $fRinksSortMode = $('#fRinks_sort_mode');
  var fRinksTimeout;

  // #region    [!]----------[ MENU ]----------[!]

  // #region [mMain]
  $('.mMain_lnk').on('click', function (e) {
    e.preventDefault();

    var $this = $(this);
    var action = $this.data('action');
    var category = $this.data('category');

    fireAnalytics(action, category);
    location = $this.attr('href');
  });

  $('.mMain_switch').on('click', function () {
    $(this).toggleClass('_active');
    $mMain.add($lOverlay).toggleClass('_show');
  });

  $lOverlay.on('click', function () {
    $(this).add($mMain).removeClass('_show');
  });
  // #endregion [mMain]

  // #region [mVote]
  $(document).on('click', '.mVote__lnk', function () {
    var $btn = $(this);
    var type = $btn.data('type');
    var rink = $btn.data('rink');
    var isDisable = $btn.hasClass('_disable');
    var isVoted = localStorage.getItem(type + rink);

    if (!isDisable) {
      if (!isVoted) {
        if (type === 'vk') {
          loginVK($btn, rink);
        } else
        if (type === 'fb') {
          if (fbToken) {
            ajaxVote($btn, {
              soc: 'fb',
              rink_id: rink,
              mode: 'social-vote',
              accessToken: fbToken,
            });
          } else {
            loginFB($btn, rink);
          }
        }
      } else {
        $pNot.open();
        $btn.addClass('_disable');
      }
    }
  });
  // #endregion [mVote]

  // #region [mNets]
  var Share = {
    vk: function () {
      var o = this;
      var url = 'https://vk.com/share.php?url=';

      url += o.url();
      o.popup(url);
    },
    ok: function () {
      var o = this;
      var url = 'https://connect.ok.ru/offer?url=';

      url += o.url();
      url += '&imageUrl=' + $('#share_img').data('ok');

      o.popup(url);
    },
    fb: function () {
      var o = this;
      var url = 'https://www.facebook.com/sharer/sharer.php?u=';

      url += o.url();
      o.popup(url);
    },
    url: function () {
      var href = location.href;
      var url = encodeURIComponent(href);
      return url;
    },
    popup: function (url) {
      window.open(url, '_blank');
    },
  };

  $('.mNets__lnk').on('click', function () {
    var $this = $(this);
    var type = $this.data('type');
    var isRink = $this.parent().hasClass('_is_rink');

    switch (type) {
      case 'vk':
        Share.vk();

        if (isRink) {
          fireAnalytics('katki_sharing_vk', 'NOPV');
        } else {
          fireAnalytics('general_sharing_vk', 'NOPV');
        }
        break;
      case 'ok':
        Share.ok();

        if (isRink) {
          fireAnalytics('katki_sharing_ok', 'NOPV');
        } else {
          fireAnalytics('general_sharing_ok', 'NOPV');
        }
        break;
      case 'fb':
        Share.fb();

        if (isRink) {
          fireAnalytics('katki_sharing_fb', 'NOPV');
        } else {
          fireAnalytics('general_sharing_fb', 'NOPV');
        }
        break;
    }

    return false;
  });
  // #endregion [mNets]

  // #region [mPages]
  $(window).on('popstate', function () {
    if (PAGE_PATH !== location.pathname) {
      fRinksUpdate();
      getRinks(false, true);
    }
  });

  $(document).on('click', '.mPages_lnk', function () {
    var page = $(this).data('page');

    if (page) {
      getRinks(page);
    }

    return false;
  });
  // #endregion [mPages]

  // #endregion [!]----------[ MENU ]----------[!]

  // #region    [!]----------[ SHOW ]----------[!]

  // #region [sRink]
  var sRink = new Swiper('.sRink_show', {
    loop: true,
    grabCursor: true,
    pagination: {
      clickable: true,
      el: '.sRink_dots',
      bulletActiveClass: '_active',
    },
    navigation: {
      prevEl: '.sRink_prev',
      nextEl: '.sRink_next',
      disabledClass: '_disabled',
    },
  });
  // #endregion [sRink]

  // #region [sDiff]
  $('.sDiff').Cocoen({
    dragCallback: function (itm, ratio) {
      var pos = ratio * 100;

      $item = $(itm);

      pos = pos.toFixed();

      $item.addClass('_active');

      if (pos < 50) {
        $item.addClass('_after').removeClass('_before');
      } else {
        $item.addClass('_before').removeClass('_after');
      }
    },
  });
  // #endregion [sDiff]

  // #endregion [!]----------[ SHOW ]----------[!]

  // #region    [!]----------[ TABS ]----------[!]
  var tabTimeout;

  function tabOpen($link, $tabs, $tab) {
    $link.addClass('_active').siblings().removeClass('_active');

    clearTimeout(tabTimeout);
    tabTimeout = setTimeout(function () {
      $tabs
        .css({
          order: 0,
          opacity: 0,
        })
        .stop()
        .delay(100)
        .slideUp(300, 'easeInOutSine')
        .queue(function () {
          $(this)
            .removeClass('_active')
            .removeAttr('style')
            .dequeue();
        });

      $tab
        .stop()
        .delay(100)
        .slideDown(300, 'easeInOutSine')
        .queue(function () {
          $(this)
            .addClass('_active')
            .dequeue();
        });
    }, 150);
  }

  // #region [tFaq]
  var $tFaqItemName = $('.tFaq_item_name');
  var $tFaqItemBody = $('.tFaq_item_body');

  $('.mFaq_lnk').on('click', function () {
    var $this = $(this);
    var $tabs = $('.tFaq_tab');
    var $tab = $tabs.eq($this.index());

    tabOpen($this, $tabs, $tab, function () {
      if (isMobile) {
        var y = $this.offset().top - 70;

        scrollTo(y, 400);
      }
    });
  });

  $tFaqItemName.on('click', function () {
    var $this = $(this);
    var $body = $this.next();
    var isActive = $this.hasClass('_active');

    $tFaqItemName.removeClass('_active');
    $tFaqItemBody
      .slideUp(300)
      .queue(function () {
        $(this)
          .removeClass('_active')
          .dequeue();
      });

    if (!isActive) {
      $this.addClass('_active');
      $body
        .stop()
        .slideDown(300)
        .queue(function () {
          $(this)
            .addClass('_active')
            .dequeue();
        });
    }
  });
  // #endregion [tFaq]

  // #endregion [!]----------[ TABS ]----------[!]

  // #region    [!]----------[ BASE ]----------[!]

  // #region [ie]
  detectIE();

  function detectIE() {
    var ua = window.navigator.userAgent;

    if (ua.indexOf('MSIE') > 0 || ua.indexOf('Trident') > 0) {
      $('html').addClass('ie');
    }
  }
  // #endregion [ie]

  // #region [hint]
  $('.js_hint').tooltipster({
    delay: 150,
    maxWidth: 300,
    theme: 'tiptoup',
    trigger: 'custom',
    animation: 'grow',
    delayTouch: [0, 600],
    triggerOpen: {
      tap: true,
      mouseenter: true,
    },
    triggerClose: {
      tap: true,
      mouseleave: true,
    },
  });

  $('.checks__state[data-tooltip-content]').tooltipster({
    theme: '',
    delay: 150,
    maxWidth: 300,
    animation: 'grow',
    delayTouch: [0, 600],
    contentCloning: true,
    side: ['top', 'left'],
    triggerOpen: {
      tap: true,
      mouseenter: true,
    },
    triggerClose: {
      tap: true,
      mouseleave: true,
    },
    functionPosition: function (instance, helper, position) {
      if (isDesktop) {
        position.coord.left -= (helper.origin.offsetWidth * 1.245);
      }

      return position;
    },
  });
  // #endregion [hint]

  // #region [hash]
  scrollToHash(PAGE_HASH);

  $('.js_scroll_to').on('click', function () {
    var href = $(this).attr('href');
    var $item = $(href);

    if ($item.length) {
      scrollTo($item.offset().top - 20, 600);
    }

    return false;
  });
  // #endregion [hash]

  // #region [flyer]
  $('.flyer_print').on('click', function () {
    fireAnalytics('katki_download', 'DL');
    window.print();
  });

  $('.flyer_menu_lnk').on('click', function () {
    var $this = $(this);
    var index = $this.index() - 1;

    $this.addClass('_active').siblings().removeClass('_active');
    $('.flyer_scene').removeClass('_active').eq(index).addClass('_active');
  });
  // #endregion [flyer]

  // #region [rinks]
  $('.rinks_item_lnk').on('click', function (e) {
    e.preventDefault();
    fireAnalytics('katki_member', 'PV');
    location = $(this).attr('href');
  });
  // #endregion [rinks]

  // #region [revise]
  var reviseSwiper = !1;
  var reviseShow = document.getElementsByClassName('revise_show');

  function reviseShowON() {
    if (!reviseSwiper && reviseShow.length) {
      reviseSwiper = new Swiper(reviseShow, {
        spaceBetween: 20,
        slidesPerView: 'auto',
        centeredSlides: true,
        pagination: {
          clickable: true,
          el: '.revise_dots',
          bulletActiveClass: '_active',
        },
      });
    }
  }

  function reviseShowOFF() {
    for (var i = 0; i < reviseSwiper.length; i++) {
      reviseSwiper[i].destroy();
    }

    reviseSwiper = false;
  }
  // #endregion [revise]

  // #region [parallax]
  var lView = document.getElementById('lView');

  if (lView) {
    var lViewParallax = new Parallax(lView);
  }
  // #endregion [parallax]

  // #region [analytics]
  $('.js_lnk_rule').on('click', function () {
    fireAnalytics('katki_rules', 'DL');
  });
  // #endregion [analytics]

  // #region [fix url images]
  if (IS_BASE) {
    var fixUrlImages = function () {
      var items = [];

      items[0] = document.querySelectorAll('.sRink_item, .sDiff_item, .rules_item, .rinks_item_view, .revise_item_box, .winners_item_img');
      items[1] = document.querySelectorAll('.flyer_scene, .flyer_rink_img');

      for (var i = 0; i < items[0].length; i++) {
        var itemOne = items[0][i];
        var itemOneAttr = itemOne.getAttribute('style').replace(TEST_URI, MAIN_PATH);

        itemOne.setAttribute('style', itemOneAttr);
      }

      for (var j = 0; j < items[1].length; j++) {
        var itemTwo = items[1][j];
        var itemTwoAttr = itemTwo.getAttribute('src').replace(TEST_URI, MAIN_PATH);

        itemTwo.setAttribute('src', itemTwoAttr);
      }
    };

    fixUrlImages();
  }
  // #endregion [fix url images]

  // #endregion [!]----------[ BASE ]----------[!]

  // #region    [!]----------[ POPUP ]----------[!]
  $('.js_popup').remodal();

  // #region [pMap]
  $('.js_open_map').on('click', function () {
    $pMap.addClass('_show');

    if (window.map) {
      window.map.container.fitToViewport();
    }
  });

  $('.pMap_close').on('click', function () {
    $pMap.removeClass('_show');
  });
  // #endregion [pMap]

  // #region [pPhoto]
  var $pPhoto = $('#pPhoto').remodal();

  $('.checks__img').on('click', function () {
    var url = $(this)
      .data('url')
      .replace(TEST_URI, MAIN_PATH);

    $('.pPhoto_img')
      .attr('src', url);

    $pPhoto.open();
  });

  $(document).on('closed', $pPhoto, function () {
    $('.pPhoto_img')
      .attr('src', 'data:,');
  });
  // #endregion [pPhoto]

  // #endregion [!]----------[ POPUP ]----------[!]

  // #region    [!]----------[ FORM CONF ]----------[!]

  // #region [fn]
  function fnFormReset($form) {
    $form[0].reset();

    if ($bday.length) {
      var $bdayYMD = $('#bday_y, #bday_m, #bday_d');
      var $bdayMD = $('#bday_m, #bday_d');

      $bdayMD
        .selectOrDie('enable', '')
        .selectOrDie('disable');
      $bdayYMD.val('');
    }

    $form.find('.eFile_lbl').html('');
    $form.find('input').iCheck('update');
    $form.find('select').selectOrDie('update');
    $form.find('._error, ._valid, ._filled').removeClass('_error _valid _filled');
  }

  function fnFormSubmit($form, mode, funcs) {
    var enctype = $form.attr('enctype');
    var $button = $form.find(':submit');
    var ajaxConfig = {};
    var data;

    if (funcs.before) {
      funcs.before();
    }

    $lLoader.fadeIn(400);
    $button.prop('disabled', true);

    if (enctype !== 'multipart/form-data') {
      data = 'mode=' + mode + '&' + $form.serialize();
    } else {
      data = new FormData($form[0]);

      data.append('mode', mode);

      ajaxConfig = {
        cache: false,
        timeout: 600000,
        processData: false,
        contentType: false,
      };
    }

    $.extend(ajaxConfig, {
      data: data,
      type: 'post',
      url: BASE_AJAX,
      dataType: 'json',
      error: function (o) {
        if (funcs.fail) {
          funcs.fail(o);
        }
      },
      success: function (o) {
        if (o.return === 'ok') {
          fnFormReset($form);

          if (funcs.done) {
            funcs.done(o);
          }
        } else if (funcs.fail) {
          funcs.fail(o);
        }
      },
      complete: function () {
        $lLoader.fadeOut(400);
        $button.prop('disabled', false);
      },
    });

    $.ajax(ajaxConfig);
  }

  function fnFormValidate(form, mode, funcs) {
    $form = $(form);

    if ($form.length) {
      $form.validate({
        errorPlacement: function () {
          return false;
        },
        submitHandler: function (form) {
          fnFormSubmit($(form), mode, funcs);
        },
      });
    }
  }
  // #endregion [fn]

  // #region [config]
  $.validator.setDefaults({
    rules: {
      'city[name]': {
        remote: {
          type: 'post',
          url: BASE_AJAX,
          data: {
            mode: 'city',
            id: function () {
              return $cityID.val();
            },
            name: function () {
              return $cityName.val();
            },
          },
        },
      },
      'region[name]': {
        remote: {
          type: 'post',
          url: BASE_AJAX,
          data: {
            mode: 'region',
            id: function () {
              return $regionID.val();
            },
            name: function (value) {
              return $regionName.val();
            },
          },
        },
      },
    },
    errorClass: '_error',
    validClass: '_valid',
    highlight: function (item, errorClass, validClass) {
      inputHighlight(this, item, errorClass, validClass);

      if (this.numberOfInvalids()) {
        $formNotify.addClass('_show');
      }
    },
    unhighlight: function (item, errorClass, validClass) {
      inputHighlight(this, item, validClass, errorClass);

      if (!this.numberOfInvalids()) {
        $formNotify.removeClass('_show');
      }
    },
    invalidHandler: function (event, validator) {
      if (validator.numberOfInvalids()) {
        $formNotify.addClass('_show');
      }
    },
  });

  $.validator.addMethod('rule_age', function (value, element) {
    var check = false;

    value = $('#bday').val();

    if (value) {
      value = value.split('.');

      var pickD = parseInt(value[0], 10);
      var pickY = parseInt(value[2], 10);
      var pickM = parseInt(value[1], 10) - 1;

      var pickDate = new Date(pickY, pickM, pickD);

      var diffDate = Math.abs(thisDate.getTime() - pickDate.getTime());
      var fullY = round(diffDate / (1000 * 3600 * 24 * 365));

      if (fullY >= 18) {
        check = true;
      }
    } else {
      check = true;
    }

    return this.optional(element) || check;
  }, 'Only adults are allowed');

  $.validator.addMethod('rule_ltrs', function (value, element) {
    return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z\s]+$/i.test(value);
  }, 'Only letters are allowed');

  $.validator.addMethod('rule_text', function (value, element) {
    return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z0-9\s\.,\-\(\)]+$/i.test(value);
  }, 'Only letters, nembers and punctuation are allowed');

  $.validator.addMethod('rule_photo', function (value, element) {
    var check = value === 'Y';

    return this.optional(element) || check;
  }, 'Incorrect number of photos');

  $.validator.addMethod('rule_phone', function (value, element) {
    return this.optional(element) || /^\+?(\d{1,3})\s?\(?\d{2,4}\)?\s?(\d{3}(-|\s)?(\d{3}|\d{2}(-|\s)?\d{2}))$/i.test(value);
  }, 'Incorrect phone');

  $.validator.addClassRules('v_age', { rule_age: true });
  $.validator.addClassRules('v_digits', { digits: true });
  $.validator.addClassRules('v_ltrs', { rule_ltrs: true });
  $.validator.addClassRules('v_text', { rule_text: true });
  $.validator.addClassRules('v_photo', { rule_photo: true });
  $.validator.addClassRules('v_phone', { rule_phone: true });

  function inputHighlight(o, item, addClass, removeClass) {
    var type = item.type;
    var $this = $(item);
    var $prnt = $this;

    if (type === 'select-one') {
      $prnt = $this.parents('.sod_select');
    } else
    if (type === 'radio' || type === 'checkbox') {
      $prnt = o.findByName(item.name).parents('.iradio, .icheckbox, .js_field_box');
    } else {
      $prnt = $this.closest('.js_field_box');
    }

    $this.add($prnt).addClass(addClass).removeClass(removeClass);
  }
  // #endregion [config]

  // #endregion [!]----------[ FORM CONF ]----------[!]

  // #region    [!]----------[ FORM FLDS ]----------[!]

  // #region [date]
  $('.js_datepicker').datepicker({
    autoClose: true,
    maxDate: new Date(),
    onSelect: function (formattedDate, date, inst) {
      inst.$el.trigger('change');
    },
  });
  // #endregion [date]

  // #region [bday]
  if ($bday.length) {
    (function () {
      var pickDate;
      var pickDateY;
      var pickDateM;
      var pickDateD;

      var $bdayY = $('#bday_y');
      var $bdayM = $('#bday_m');
      var $bdayD = $('#bday_d');

      var thisDateD = thisDate.getUTCDate();
      var thisDateY = thisDate.getUTCFullYear();
      var thisDateM = thisDate.getUTCMonth() + 1;

      var fillItemM = function (max) {
        var month;
        var monthList = '<option value="">MM</option>';

        for (var i = 0; i <= max; i++) {
          month = ('0' + (i + 1)).slice(-2);
          monthList += '<option value="' + month + '">' + month + '</option>';
        }

        $bdayM
          .html(monthList)
          .selectOrDie('enable')
          .selectOrDie('update');

        $bdayD
          .html('<option value="">DD</option>')
          .selectOrDie('disable')
          .selectOrDie('update');
      };

      var fillItemD = function (max) {
        var day;
        var dayList = '<option value="">DD</option>';

        for (var i = 1; i <= max; i++) {
          day = ('0' + i).slice(-2);
          dayList += '<option value="' + day + '">' + day + '</option>';
        }

        $bdayD
          .html(dayList)
          .selectOrDie('enable')
          .selectOrDie('update');
      };

      $bdayY.on('change', function () {
        var numM = 11;

        pickDateY = $(this).val();
        $('.js_bday_itm, .js_bday_box').removeClass('_error');

        if (pickDateY === thisDateY) {
          numM = thisDateM;
        }

        $bdayY.selectOrDie('disable', '');
        $bday.val('');
        fillItemM(numM);
      });

      $bdayM.on('change', function () {
        var numD = thisDateD;

        pickDateM = $(this).val();
        $('.js_bday_itm, .js_bday_box').removeClass('_error');

        if (pickDateY !== thisDateY) {
          numD = new Date(pickDateY, pickDateM - 1).daysInMonth();
        }

        $bdayM.selectOrDie('disable', '');
        $bday.val('');
        fillItemD(numD);
      });

      $bdayD.on('change', function () {
        pickDateD = $(this).val();
        pickDate = pickDateD + '.' + pickDateM + '.' + pickDateY;

        $bdayD.selectOrDie('disable', '');
        $bday.val(pickDate).valid();
        $('.js_bday').valid();
      });
    }());
  }
  // #endregion [bday]

  // #region [file]
  $('.js_file').on('change', function () {
    var $this = $(this);
    var $prnt = $this.parent();
    var $inpt = $prnt.next();
    var files = this.files;
    var sum = files.length;
    var min = $this.data('min');
    var max = $this.data('max');
    var minw = $this.data('minw');
    var minh = $this.data('minh');
    var size = $this.data('size');
    var $label = $this.next();

    if (min) {
      $label.html(sum || '');
    }

    checkPhotos($inpt, files, sum, min, max, minw, minh, size);

    if (sum) {
      $prnt.addClass('eFile-filled');
    } else {
      $prnt.removeClass('eFile-filled');
    }
  });

  function checkPhotos($inpt, files, sum, min, max, minw, minh, size) {
    var index = 0;
    var check = 'N';

    min = min || 1;
    max = max || 1;

    var setRes = function (val) {
      $inpt
        .val(val)
        .valid();
    };

    var checkSize = function () {
      for (var i = 0; i < sum; i++) {
        var fileSize = round(files[i].size / 1000000, 1);
        var isWrong = fileSize > size;

        if (isWrong) {
          check = 'N';
        }
        if (isWrong) break;
      }
    };

    var checkSizes = function (fileIndex) {
      if (fileIndex < sum) {
        var img = new Image();

        img.onload = function () {
          var w = this.width;
          var h = this.height;
          var isRight = (
            w >= minw
              && h >= minh
              && Math.abs(w / h - 16 / 9) >= 0
          );

          if (isRight) {
            checkSizes(fileIndex + 1);
          } else {
            check = 'N';
          }

          if (check === 'N' || fileIndex === sum - 1) {
            setRes(check);
          }
        };

        img.src = window.URL.createObjectURL(files[index]);
      }
    };

    if (sum >= min && sum <= max) {
      check = 'Y';

      checkSize();

      if (files.length === 1) {
        setRes(check);
      } else
      if (check === 'Y') {
        checkSizes(index);
      } else {
        setRes(check);
      }
    } else {
      setRes(check);
    }
  }
  // #endregion [file]

  // #region [phone]
  $('.js_phone').mask('+7 (000) 000-00-00');
  // #endregion [phone]

  // #region [input]
  $('.eField_input').on('change', function () {
    var $this = $(this);
    var value = $this.val().trim();

    $this.val(value);

    if (value) {
      $this.addClass('_filled');
    } else {
      $this
        .text('')
        .attr('value', '')
        .removeClass('_filled');
    }

    if (!value && !$this.is(':required')) {
      $this.removeClass('_error _valid');
    }
  });
  // #endregion [input]

  // #region [select]
  $('.lPage select').selectOrDie();
  $('.lPage form').on('reset', function () {
    $('.lPage select').selectOrDie('update');
  });
  // #endregion [select]

  // #region [address]
  var $cityID = $('#city_id');
  var $cityName = $('#city_name');
  var $regionID = $('#region_id');
  var $regionName = $('#region_name');

  function removeKladrAdv() {
    $('#kladr_autocomplete li:first-child').remove();
  }

  $('.js_city').fias({
    verify: true,
    typeCode: 'city',
    type: $.fias.type.city,
    parentInput: '.js_region',
    open: removeKladrAdv,
    check: function (o) {
      $cityID.val(o.id);
    },
    select: function (o) {
      $cityID.val(o.id);
    },
  });

  $('.js_region').fias({
    verify: true,
    type: $.fias.type.region,
    open: removeKladrAdv,
    check: function (o) {
      $regionID.val(o.id);
    },
    select: function (o) {
      $regionID.val(o.id);
      $('.js_city').val('').trigger('change');
    },
  });
  // #endregion [address]

  // #region [required fields]
  var $requiredFields = $('.js_required_fields');

  $requiredFields.on('change', function () {
    var isFieldsFilled = false;
    var values = $requiredFields.serialize().split('&');

    for (var i = 0; i < values.length; i++) {
      isFieldsFilled = values[i].slice(-1) !== '=';

      if (isFieldsFilled) {
        break;
      }
    }

    if (isFieldsFilled) {
      $requiredFields.attr('required', true);
    } else {
      $requiredFields.attr('required', false);
      $requiredFields.valid();
    }
  });
  // #endregion [required fields]

  // #region [radio & checkbox]
  $('.lPage :radio, .lPage :checkbox').not('._simple')
    .iCheck({
      hoverClass: '_hover',
      checkedClass: '_checked',
      disabledClass: '_disabled',
    })
    .on('ifChanged', function () {
      $(this).closest('form').trigger('change');
    })
    .filter('[required]')
    .on('ifChanged', function () {
      $(this).valid();
    });
  // #endregion [radio & checkbox]

  // #endregion [!]----------[ FORM FLDS ]----------[!]

  // #region    [!]----------[ FORM LIST ]----------[!]

  // #region [fMain]
  $('#fMain').attr('autocomplete', 'off');

  if (userIP) {
    $('#user_ip').val(userIP);
  }

  fnFormValidate('#fMain', 'add-rink', {
    done: function (o) {
      $pCreate.open();

      // firePixels('CompleteRegistration');
      fireAnalytics('katki_send_OK', 'NOPV');

      var _rutarget = window._rutarget || [];

      _rutarget.push({
        event: 'thankYou',
        conv_id: 'regist',
        order_id: o.orderId,
      });
    },
    before: function () {
      fireAnalytics('katki_send', 'NOPV');
    },
  });
  // #endregion [fMain]

  // #region [fCheck]
  fnFormValidate('#fCheck', 'add-check', {
    done: function () {
      $pCheckAdded.open();
    },
  });
  // #endregion [fCheck]

  // #region [fBeep]
  fnFormValidate('#fBeep', 'feedback', {
    done: function () {
      $pFeedback.open();
      fireAnalytics('feedback_send_OK', 'NOPV');
    },
    before: function () {
      fireAnalytics('feedback_send', 'NOPV');
    },
  });
  // #endregion [fBeep]

  // #region [fRinks]
  fRinksUpdate();

  $fRinks
    .on('submit', function () {
      return false;
    })
    .on('change', function () {
      clearTimeout(fRinksTimeout);
      fRinksTimeout = setTimeout(getRinks, 200);
    });

  $fRinksCity
    .fias({
      verify: true,
      typeCode: 'city',
      type: $.fias.type.city,
      open: removeKladrAdv,
      check: function (o) {
        $fRinksCityID.val(o.id);
        $fRinks.trigger('change');
      },
      select: function (o) {
        $fRinksCityID.val(o.id);
        $fRinks.trigger('change');
      },
    })
    .on('change', function () {
      var $this = $(this);

      if (!$this.val()) {
        $fRinksCityID.val('');
        $fRinks.trigger('change');
      }
    });

  $('.fRinks_type_lnk').on('click', function () {
    var $this = $(this);
    var value = $this.data('value');

    if (!$this.hasClass('_active')) {
      fRinksReset();

      $fRinksType.val(value);

      $fRinks.trigger('change');
      $this.addClass('_active').siblings().removeClass('_active');

      if (value === 1) {
        fireAnalytics('katki_large_city', 'NOPV');
      } else {
        fireAnalytics('katki_small_city', 'NOPV');
      }
    }
  });

  $('.fRinks_sort_lnk').on('click', function () {
    var $this = $(this);
    var value = $this.data('value');
    var mode = 'asc';

    if ($this.hasClass('_asc')) {
      mode = 'desc';
    }

    $fRinksSortType.val(value);
    $fRinksSortMode.val(mode);
    $fRinks.trigger('change');

    if ($this.hasClass('_active')) {
      $this.toggleClass('_asc _desc');
    } else {
      $this.addClass('_asc _active').siblings().removeClass('_asc _desc _active');
    }
  });

  function fRinksReset() {
    $fRinks[0].reset();

    $('.fRinks_sort_lnk')
      .eq(0)
      .addClass('_asc _active')
      .siblings()
      .removeClass('_asc _desc _active');
  }
  function fRinksUpdate() {
    var query = location.search;
    var prms = {}; var prm;
    var $fld; var $lnk;

    if (query && $fRinks.length) {
      query = query.substr(1).split('&');

      for (var i = 0; i < query.length; i++) {
        prm = query[i].split('=');
        prm[1] = decodeURIComponent(prm[1]);

        prms[prm[0]] = prm[1];

        $fld = $fRinks.find('[name="' + prm[0] + '"]');
        $lnk = $fRinks.find('[data-name="' + prm[0] + '"][data-value="' + prm[1] + '"]');

        if ($fld.length) {
          $fld.val(prm[1]);
        }
        if ($lnk.length) {
          $lnk.addClass('_active').siblings().removeClass('_asc _desc _active');
        }
      }

      $('.fRinks_sort_lnk._active').addClass('_' + prms.sort_mode);
    }
  }
  function getRinks(page, isStory) {
    if ($fRinks.length) {
      var $form = $('#fRinks');
      var formY = $form.offset().top || 0;
      var ajaxData = '';
      var pageUrl = '';

      scrollTo(formY, 600);

      if (isStory) {
        ajaxData = location.search.substr(1);
      } else {
        if (page) {
          ajaxData = (page > 1) ? ('page=' + page) : '';
          ajaxData += (ajaxData !== '') ? '&' : '';
        }

        ajaxData += $form.serialize();
        ajaxData = ajaxData.replace(/&?[^=&]+=(&|$)/g, '&');
        ajaxData = ajaxData.slice(0, -1);

        pageUrl = (ajaxData === '')
          ? PAGE_PATH
          : PAGE_PATH + '?' + ajaxData;

        history.pushState(null, null, pageUrl);
      }

      $lLoader.fadeIn(400);
      ajaxData = 'mode=get-rinks-by-city&' + ajaxData;

      $.ajax({
        type: 'post',
        url: BASE_AJAX,
        data: ajaxData,
        dataType: 'json',
        complete: function () {
          $lLoader.fadeOut(400);
        },
        success: function (data) {
          fillTemplate(data);
        },
      });
    }
  }
  // #endregion [fRinks]

  // #endregion [!]----------[ FORM LIST ]----------[!]

  // #region    [!]----------[ RESPONSE ]----------[!]
  var checkScreenSizeON = !1;

  function enableMobile() {
    reviseShowON();
    mapScrollZoomOff();

    $('.rink_head').prependTo('.rink_side');
  }
  function enableDesktop() {
    reviseShowOFF();
    mapScrollZoomOn();

    $('.rink_head').prependTo('.rink_base');
  }
  function checkScreenSize() {
    if (window.matchMedia(szMobile).matches) {
      if (!isMobile) {
        isMobile = true;
        isDesktop = false;

        enableMobile();
      }
    } else
    if (!isDesktop) {
      isMobile = false;
      isDesktop = true;

      enableDesktop();
    }

    checkScreenSizeON = !1;
  }

  checkScreenSize();

  $(window).on('resize.check', function () {
    if (!checkScreenSizeON) {
      checkScreenSizeON = true;

      requestAnimationFrame(function () {
        checkScreenSize();
      });
    }
  });
  // #endregion [!]----------[ RESPONSE ]----------[!]

  // #region    [!]----------[ FUNCTION ]----------[!]
  function scrollShow() {
    $('html').css({ overflow: '' });
  }
  function scrollHide() {
    $('html').css({ overflow: 'hidden' });
  }
  function getItemY(item) {
    return item.getBoundingClientRect().top + window.pageYOffset;
  }
  function fillTemplate(o) {
    $ajaxOutput.html(
      tmpl('tmpl_output', o)
    );

    if ('pages' in o) {
      $mPages.html(
        tmpl('tmpl_pages', o.pages)
      );
    } else {
      $mPages.html('');
    }
  }
  function round(value, decimals) {
    decimals = decimals || 0;
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
  }
  function scrollToHash(hash) {
    var y; var item;

    if (hash) {
      item = document.getElementById(hash.substr(1));

      window.scrollTo(0, 0);

      if (item) {
        y = getItemY(item) - 20;
        scrollTo(y, 600);
      }
    }
  }
  function scrollTo(itemY, duration, callback) {
    var winY = window.pageYOffset;
    var diff = itemY - winY;
    var start;

    window.requestAnimationFrame(function step(timestamp) {
      if (!start) {
        start = timestamp;
      }

      var time = timestamp - start;
      var easing = function (t) {
        return t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
      };
      var percent = easing(
        Math.min(time / duration, 1)
      );
      var y = winY + diff * percent;

      window.scrollTo(0, y);

      if (time < duration) {
        window.requestAnimationFrame(step);
      } else if (typeof callback === 'function') {
        callback();
      }
    });
  }

  $.extend($.easing, {
    easeInSine: function (e, n, t, a, u) {
      return -a * Math.cos((n / u) * (Math.PI / 2)) + a + t;
    },
    easeOutSine: function (e, n, t, a, u) {
      return a * Math.sin((n / u) * (Math.PI / 2)) + t;
    },
    easeInOutSine: function (e, n, t, a, u) {
      return (-a / 2) * (Math.cos((Math.PI * n) / u) - 1) + t;
    },
  });

  Date.prototype.daysInMonth = function () {
    return 33 - new Date(this.getFullYear(), this.getMonth(), 33).getDate();
  };
  // #endregion [!]----------[ FUNCTION ]----------[!]



  // #region    [!]----------[ VOTING ]----------[!]
  (function (d, s, id) {
    var js; var fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  window.fbAsyncInit = function () {
    FB.init({
      xfbml: true,
      cookie: true,
      version: 'v2.6',
      appId: FB_APP_ID,
    });

    FB.getLoginStatus(function (o) {
      if (o.status === 'connected') {
        fbToken = o.authResponse.accessToken;

        $('.mVote__lnk_fb').addClass('_auth');
      }
    });
  };

  function loginVK($btn, rink) {
    var uri;
    var popup;
    var check;
    var timeout;

    uri = 'http://oauth.vk.com/authorize?client_id=' + VK_APP_ID;
    uri += '&redirect_uri=' + TEST_VOTE_URI + '?data=vk-' + rink + '-' + userIP;
    uri += '&response_type=code&display=popup&v=5.92';

    if (isMobile) {
      popup = window.open('', '_blank');
    } else {
      popup = window.open('', 'auth', 'toolbar=0,status=0,width=626,height=436');
    }

    ajaxVote($btn, {
      soc: 'vk',
      rink_id: rink,
      mode: 'check-vote',
    }, function ($btn, net, rink) {
      popup.location = uri;

      check = function (popupInst) {
        var isAuth = getCookie('pf_USER_ID_vk');
        var isError = getCookie('pf_rink_vk_' + rink);

        if (popupInst) {
          if (isError) {
            // popupInst.close();
            // popupInst = null;

            $pNot.open();

            deleteCookie('pf_rink_vk_' + rink);
          } else if (popupInst.closed) {
            if (isAuth) {
              saveVote(net, rink);

              // vk logout ;)
              deleteCookie('remixlhk');
              deleteCookie('remixsid');
              deleteCookie('remixlang');
            } else {
              $btn.removeClass('_disable');
            }
          } else {
            clearTimeout(timeout);
            timeout = setTimeout(function () {
              check(popupInst);
            }, 500);
          }
        }
      };

      check(popup);
    }, function () {
      if (popup) {
        // popup.close();
        // popup = null;
      }
    });
  }

  function loginFB($btn, rink) {
    FB.login(function (o) {
      if (o.authResponse) {
        fbToken = o.authResponse.accessToken;

        ajaxVote($btn, {
          soc: 'fb',
          rink_id: rink,
          mode: 'social-vote',
          accessToken: fbToken,
        });
      }
    }, {
      scope: 'public_profile,email',
    });
  }

  function saveVote(net, rink) {
    var $item = $('.js_score_' + rink);
    var score = parseInt($item.text(), 10) + 1;
    var label = wordnum(score, ['голос', 'голоса', 'голосов']);

    $pThx.open();
    localStorage.setItem(net + rink, 1);
    $item.html(score).attr('data-txt', label);

    // firePixels('SubmitApplication');
    fireAnalytics('katki_voting_' + net + '_OK', 'NOPV');
  }

  function ajaxVote($btn, data, fnDone, fnFail) {
    var net = data.soc;
    rink = data.rink_id;

    if (userIP) { data.ip = userIP; }

    fireAnalytics('katki_voting_' + net, 'NOPV');

    $.ajax({
      data: data,
      type: 'post',
      // cache: false,

      //dataType: 'jsonp',
      //jsonp: 'mycallbackfunction',
      //crossDomain: true,
      //url: 'https://www.nivea.ru' + MAIN_PATH + 'social/social_redirect.php',

      url: VOTE_URI,
      dataType: 'json',
      error: function () {
        if (typeof fnFail === 'function') {
          fnFail();
        }
      },
      success: function (o) {
        if (/ok|error/.test(o.return)) {
          $btn.addClass('_disable');
        }

        if (o.return === 'ok') {
          if (net === 'fb') {
            saveVote(net, rink);
          } else
          if (net === 'vk' && typeof fnDone === 'function') {
            fnDone($btn, net, rink);
          }
        } else {
          $pNot.open();

          if (typeof fnFail === 'function') {
            fnFail();
          }
        }
      },
    });
  }
  // #endregion [!]----------[ VOTING ]----------[!]

  // #region    [!]----------[ COOKIE ]----------[!]
  function getCookie(name) {
    name = name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1');

    var matches = document.cookie.match(
      new RegExp('(?:^|; )' + name + '=([^;]*)')
    );

    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

  function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires === 'number' && expires) {
      var d = new Date();
      d.setTime(d.getTime() + expires * 1000);
      expires = options.expires = d;
    }

    if (expires && expires.toUTCString) {
      options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + '=' + value;

    for (var key in options) {
      if (options.hasOwnProperty(key)) {
        updatedCookie += '; ' + key;

        var optionValue = options[key];

        if (optionValue !== true) {
          updatedCookie += '=' + optionValue;
        }
      }
    }

    document.cookie = updatedCookie;
  }

  function deleteCookie(name) {
    setCookie(name, '', {
      expires: -1,
    });
  }
  // #endregion [!]----------[ COOKIE ]----------[!]

}(jQuery));
