const {
  src,
  dest,
  watch,
  series,
  parallel,
} = require('gulp');

const image = require('gulp-image');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const repath = require('gulp-replace-path');
const postcss = require('gulp-postcss');
const favicons = require('gulp-favicons');

const bsync = require('browser-sync').create();

const bsyncStream = bsync.stream;
const bsyncReload = bsync.reload;

const PATHS = {
  js: 'assets/js/',
  css: 'assets/css/',
  img: 'assets/img/',
  font: 'assets/font/',
  data: 'assets/data/',
  media: 'assets/media/',
  icons: 'assets/icons/',
};

function em(px, base) {
  base = base ? base : 10;
  return round((px / base) + 0.0001, 4);
}

function round(value, decimals) {
  return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

function taskJs(done) {
  src([
    'source/js/jquery.min.js',
    'source/js/plugin.min.js'
  ], {
    sourcemaps: true,
  })
    // .pipe(uglify())
    .pipe(concat('bundle.min.js'))
    .pipe(
      dest(PATHS.js, {
        sourcemaps: '.',
      })
    )
    .pipe(bsyncStream());

  src('source/js/script.js')
    .pipe(
      dest(PATHS.js, {
        sourcemaps: '.',
      })
    )
    .pipe(bsyncStream());

  done();
}

function taskCss(done) {
  src('source/css/index.css', {
    sourcemaps: true,
  })
    .pipe(rename('style.min.css'))
    .pipe(postcss())
    .pipe(repath(/\/assets\//g, '../'))
    .pipe(
      dest(PATHS.css, {
        sourcemaps: '.',
      })
    )
    .pipe(bsyncStream());

  done();
}

function taskImg(done) {
  src('source/img/**/*')
    .pipe(
      image({
        quiet: true,
        concurrent: 10,

        svgo: true,

        optipng: false,
        pngquant: true,
        zopflipng: true,

        mozjpeg: true,
        guetzli: false,
        jpegRecompress: false,

        gifsicle: true,
      })
    )
    .pipe(dest(PATHS.img));

  done();
}

function taskFavicon(done) {
  src('source/img/favicon.{svg,png}')
    .pipe(
      favicons({
        replace: true,
        pipeHTML: false,
        html: 'index.html',
        path: '/assets/icons/',

        icons: {
          firefox: false,
          favicons: true,
          windows: false,
          yandex: false,
          coast: false,
          android: false,
          appleIcon: false,
          appleStartup: false,
        },
      })
    )
    .pipe(dest(PATHS.icons));

  done();
}

function taskFontFiles(done) {
  src('source/font/*.*')
    .pipe(dest(PATHS.font))
    .pipe(bsyncStream());

  done();
}

function taskDataFiles(done) {
  src('source/data/**/*.*')
    .pipe(dest(PATHS.fonts))
    .pipe(bsyncStream());

  done();
}

function taskMediaFiles(done) {
  src('source/media/**/*.*')
    .pipe(dest(PATHS.media))
    .pipe(bsyncStream());

  done();
}

function taskServer(done) {
  bsync.init({
    notify: false,
    server: {
      baseDir: './',
    },
  });

  watch(
    [
      '*.html',
      'source/json/**/*.*'
    ], {
      events: [
        'add',
        'change'
      ],
    },
    function (cb) {
      bsyncReload();
      cb();
    }
  );

  watch(
    'source/js/**/*.js',
    { events: 'change' },
    taskJs
  );

  watch(
    'source/css/**/*.css',
    { events: 'change' },
    taskCss
  );

  watch(
    'source/img/**/*.{svg,png,jpg,gif}',
    { events: ['add', 'change'] },
    taskImg
  );

  watch(
    'source/fonts/*.*',
    { events: 'change' },
    taskFontFiles
  );

  watch(
    'source/media/**/*.*',
    { events: ['add', 'change'] },
    taskMediaFiles
  );

  done();
}

exports.js = parallel(taskJs);
exports.css = parallel(taskCss);
exports.img = parallel(taskImg);
exports.ico = parallel(taskFavicon);

exports.default = series(
  taskImg,
  parallel(
    taskJs,
    taskCss,
    taskFontFiles,
    taskMediaFiles
  ),
  taskServer
);
